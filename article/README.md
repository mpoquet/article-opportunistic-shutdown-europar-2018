This directory contains files to generate the article.

# Dependencies
- A latex compiler
- rubber (latex build system)
- [asymptote (script-based vector graphics language compiler),
   if one wants to regenerate non-data figures]

# Generate the article
``` bash
make
```
