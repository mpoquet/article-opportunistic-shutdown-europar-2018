% This is LLNCS.DEM the demonstration file of
% the LaTeX macro package from Springer-Verlag
% for Lecture Notes in Computer Science,
% version 2.4 for LaTeX2e as of 16. April 2010
%
\documentclass{llncs}

\usepackage{xcolor}
\usepackage[utf8]{inputenc}
\usepackage{subfig}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage[vlined, algoruled,algosection]{algorithm2e}
\usepackage[inline]{enumitem}
\usepackage[binary-units]{siunitx}
\usepackage{afterpage}

% Définition de commandes pour rajouter des notes simplement (honteusement
% volé du papier de David)
\usepackage[draft]{fixme}
\newcommand{\eg}{e.g.\@\xspace}
\definecolor{pfcolor}{HTML}{1B9E77}
\definecolor{micolor}{HTML}{D95F02}
\definecolor{decolor}{HTML}{7570B3}
\definecolor{recolor}{HTML}{E7298A}
\fxsetup{theme=color,mode=multiuser,layout=inline,draft}
\FXRegisterAuthor{pf}{aspf}{\color{pfcolor}\bf PF}
\FXRegisterAuthor{mi}{asmi}{\color{micolor}\bf Millian}
\FXRegisterAuthor{de}{asde}{\color{decolor}\bf Denis}
\FXRegisterAuthor{re}{asre}{\color{recolor}\bf Reviewer}

\newcommand{\computing}{computing}
\newcommand{\idle}{idle}
\newcommand{\off}{of\!f}
\newcommand{\on}{on}
\newcommand{\ontooff}{\on\rightarrow\off}
\newcommand{\offtoon}{\off\rightarrow\on}

\begin{document}

\newcommand{\overbar}[1]{\mkern 1.5mu\overline{\mkern-1.5mu#1\mkern-1.5mu}\mkern 1.5mu}


\title{Opportunistic Shutdown in EASY Backfilling: What About Performance?}

\author{Pierre-François~Dutot\inst{1}\and
Millian~Poquet\inst{1}\and
Denis~Trystram\inst{1}}

\institute{Univ. Grenoble-Alpes, CNRS, Inria, LIG, FRANCE\\
\email{firstname.lastname@imag.fr}}

\maketitle
%\minote{On peut ajouter des notes simplement.}
%\pfnote{Et savoir qui les a ajoutées.}
%\denote{And again.}

\begin{abstract}
The evolution of large scale clusters comes with the hard challenge of mastering the energy consumption.
The purpose of this paper is to study this problem from the side of the resource manager.
We consider the mechanism of switching-off and on the processors as the main
source of energy savings.

The idea of opportunistically switching-off the machines that remain idle for
too long is common in the literature. However, the impact of this strategy
on performance is hardly studied and we propose here to investigate it from
a bi-objective optimization problem, which produces a Pareto set on the
(energy, performance) space.
We study the impact of this technique upon the classical EASY backfilling.
An extensive simulation campaign on actual traces of the \textit{Parallel Archive} is conducted.
The results show that significant gains in term of energy savings can be achieved, but that may be at the cost of non-negligible performance drops.
Consequently, we advise to use this technique with caution.

%\keywords{batch scheduling, energy minimization, bi-objective optimization, online}
\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
\label{sec:intro}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% \subsection{Context and motivation}

While the fastest High Performance Computing (HPC) systems are
evolving to extreme-scale platforms, there is a large adoption of
cluster and cloud computing by all sorts of industries for their
production needs. In constrast to the research centers at the forefront of
innovation with an almost unlimited budget, these companies do not
attempt to leverage the most floating point operations per second out
of their clusters to be ranked first in a global race but to
efficiently spend their computing budget to get the most out of their
necessarily limited needs.

One of the most promising way of reducing the expenses tied to a
cluster is to reduce its power consumption.
To begin with, the power consumption becomes a constraint since electricity companies
set upper bounds on the power that they can provide for clusters at
different periods of time.
Actually the problem not only lies in the available power, but also on the huge energy cost
(several years of intensive use may be greater than the price of the platform
itself).
It is therefore mandatory to develop efficient tools to utilize new HPC clusters at a
sustained performance rate with a lower energy consumption.

While speed-scaling is the natural way to save energy while running a
job, switching-off nodes is the natural way to save energy between
jobs.  However, most of the existing resource management algorithms
focus only on performance and energy savings are only an
afterthought. Taking into account node shutdowns at the resource
manager level while scheduling the jobs can potentially yield
significant improvements, as idle periods can be lined up to turn off
fewer nodes for longer periods of time.
%
Opportunistic shutdown is used frequently in the literature ---
\eg, in~\cite{benoit:hal-01557025,dutot2017towards} --- to reduce the energy consumption of computing platforms. Its main idea is to switch-off machines that remain idle for too long.
In this paper, we propose to investigate the interactions of this technique with the classical online scheduling algorithm EASY backfilling.

We consider this problem as a bi-objective optimization whose purpose is to
determine a good trade-off between energy and performance.
Performance will be measured as it is common by user satisfaction metrics such as mean waiting time and mean slowdown (objectives and metrics will be precisely defined later).

We simulated various executions of the opportunistic shutdown technique on top of EASY backfilling on real traces.
The technique alone does allow to achieve significant energy savings (up to 30\%), but may incur non-negligible performance drops depending on the workload.

% \subsection{Roadmap}
The paper is organized as follows.
We start by a precise description of the problem in section~\ref{sec:problem}.
Then, we describe in section~\ref{sec:techniques} the energy saving techniques
that will serve in our algorithms.
Section~\ref{sec:algorithms} presents several algorithms.
The simulation campaign and its results are reported in section~\ref{sec:evaluation}.
Related works are given in section~\ref{sec:related_works}.
Finally, we conclude and give future works in section~\ref{sec:conclusion}.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Problem Description}
\label{sec:problem}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\paragraph{Job characteristics}
We consider jobs that are parallel and rigid~\cite{feitelson2015workload},
and are submitted on-line.
Jobs are indexed by $j$ and are characterized by their release time
(denoted by $r_j$), their number of requested computing resources ($q_j$), and
their wall-time ($wall_j$).
Both $q_j$ and $wall_j$ are specified by the users and are not known
to the scheduler before $r_j$. The processing time (denoted by $p_j$)
remains unknown until the job is completed.
Job $j$ is killed if it reaches its wall-time, without any penalty on the scheduler
(in this case we simply define $p_j$ as equal to $wall_j$).
The jobs are not preemptive, which means that once started a job cannot be
interrupted until its completion.
Once the jobs have been executed, more information about them can be defined.
We denote by $start_j$ the time at which $j$ starts being executed, and $C_j$ the
time at which it completes ($C_j=start_j + p_j$).
$wait_j = start_j - r_j$ denotes the amount of time $j$ stayed in the system
before being executed. $slowdown_j = \frac{1}{p_j} (C_j - r_j)$ denotes the slowdown~\cite{feitelson2001metrics} of job $j$.


\paragraph{About the platform}
We focus on homogeneous computing platforms, whose
computing resources will be simply referred to as \textit{machines}.
A platform is defined by a set of $m$ machines.
The interconnection network is not explicitly taken into account.
The machines have various states
$S$ = $\{$ $\computing$,
           $\idle$,
           $\off$,
           $\ontooff$,
           $\offtoon$
$\}$.
Idle machines can be switched-off, which takes a time $t_{\ontooff}$.
Off machines can be switched-on, which takes a time $t_{\offtoon}$.
At a given time $t$, machine $i$ is in one and only one of the previous states.
As it is common in HPC, only $idle$ machines can be selected to compute new jobs --- machines are affected to unique jobs.
An instantaneous power consumption is associated to each state,
denoted by $P_{s}\ \forall s \in S$, and it is expressed in watts.
The electrical power consumption of a machine is entirely defined by
its state. With our simulation tool, it is pretty straightforward to
include many different computing states to reflect the dynamic voltage
and frequency scaling mechanism (DVFS). However, in order to have a
fair assessment of the improvements brought by our policies, we
considered that for every simulation the jobs had the exact same power
profile, \textit{i.e.} every job has the same duration and power consumption
regardless of when and where it is scheduled. Therefore, precisely
modeling DVFS would only change a constant fraction of the power
consumption for every simulation.


\paragraph{Objectives}
We are interested in the following bi-objective optimization problem:
minimize both the system performance and the consumed energy of the
platform.
As this is a bi-objective problem, we will provide a set of solutions that are
not completely comparable (Pareto set)~\cite{dutot2009multi}.
In this paper, we will measure performance with two user satisfaction metrics.
First, the jobs mean waiting time is defined by $\frac{1}{n} \sum_{j}{wait_j}$,
and is expressed in seconds. The mean waiting time has the main advantage of being straightforward to interpret, but has the main drawback of considering that all jobs should wait for the same amount of time. For this reason, we also use the mean slowdown defined by $\frac{1}{n} \sum_{j}{slowdown_j}$ as
a user satisfaction metrics.
The total consumed energy of the platform is defined as the sum of the consumed
energy of its machines, and is expressed in joules.
The energy consumed by a machine is defined as usual as the integral of its
power over time. The time boundaries which interest us here are between the
submission time of the first job and the completion time of the last one.
%
This bi-objective problem is to determine a trade-off ---
as both objectives are conflicting.
More precisely, the solutions that do not worsen too much the system
unresponsiveness are those which are the most relevant.
As the problem is most likely NP-hard and that targeted sets of jobs and
platforms may be huge, we are not looking for Pareto optimal solutions but for
reasonable and useful alternatives.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Energy Minimization Techniques}
\label{sec:techniques}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In order to reduce the energy cost, various techniques are used in the
literature.
These techniques can be split in two parts.
First there are techniques which aim at reducing the
energy consumption of the jobs themselves.
Second, they focus on a better resource exploitation to avoid energy wastes.
As this paper focuses on the impact of the opportunistic shutdown technique,
we consider that the first kind of techniques are fully used at the job level.


\paragraph{DVFS}
The dynamic voltage and frequency scaling (DVFS) mechanism, also known as
speed-scaling, is probably the most studied technique to reduce the jobs energy
consumption, especially in theory~\cite{albers2010energy}.
It can be used to ensure that power-limited systems do not exceed a power
threshold~\cite{georgiou_adaptive_2015}.
It can be used to reduce the power consumption of jobs, but it may also increase
the job energy consumption as the job might consume less power but for a longer
period of time~\cite{cho2010interplay}.
%
In this article, we suppose that jobs are energy efficient.
It means that we consider that DVFS decisions are done within the jobs and not
at the resource management level. This approach may reduce the energy
optimization space, but it also prevents bad DVFS decisions that might result
in increased jobs energy consumption~\cite{snowdon2005power}.

\paragraph{Exploiting Idle Time}
On current hardware, idle machines consume a lot of energy. As this part of the
energy is not used for computing jobs, we consider it as lost. Hence,
reducing this part of the energy consumption is crucial and a big source of gain.
%
One way to save energy is to switch idle machines into lower consumption
states. This mechanism greatly reduces the machines idle power
consumption but it should be used with caution as switching to
and from lower consumption states has both time and energy costs,
and potentially ages the hardware.
%
Lower consumption states can be different types of sleep states or complete
shutdowns. For the sake of readability, we will
simply refer to machines in lower consumption states as $\off$. However, please notice that our approach is not specific to this type of lower consumption state.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Algorithms}
\label{sec:algorithms}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The algorithms studied in this article are event-based.
The events may concern job submissions, job completions, or state
modifications of machines (\eg, some machines just finished to be switched-off).
In order to allow more energy savings, the algorithms are also called every $T$ seconds. Such periodic calls allows to switch-off some machines in the middle of the execution of a long job.
Whenever they are called, the algorithms take decisions, which are performed
immediately after the decision making.
Decisions include starting the execution of a job on some machines,
or switching the state of some machines (\eg, switch-on some machines).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Easy Backfilling}
\label{subsec:easyBF}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Easy Backfilling~\cite{easybackfill}, that will simply be referred to as
EASY in the remainder of this article, is one of the mostly used resource
management algorithm. This popularity can be explained by the ease of
implementation and the high utilization its aggressive
backfilling mechanism induces, since most HPC center administrators often want
to maximize the resource utilization.

EASY stores the pending jobs in a queue, which is ordered by job arrival time.
First, EASY scans the job queue in order as long as the jobs can be executed right
away. This part of the algorithm is the same as the First Come First Serve policy
(FCFS).
If a scanned job cannot be executed right away, a reservation is done for
it and the queue scanning is stopped. Such a job is called a \textit{priority}
job.
Second, EASY scans the remaining jobs in the queue (still in order) and attempts
to \textit{backfill} them, \textit{i.e.} execute them forthwith if and only if they
do not seem to delay the starting of the priority job.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Opportunistic Shutdown}
\label{subsec:opportunistic}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
What we call opportunistic shutdown is a type of machine shutdown
technique. The main idea behind it is to switch-off machines that remain idle
for more than $t_{idle}$ seconds. For this purpose, the algorithm must keep track of the machine states over time.
Machines that are switched-off this way can be used by the resource manager to compute jobs. Such machines just need to be switched back to a computing state before starting executing the job, which can take time.

\paragraph{Implementation issues}
The idea behind this technique is very simple, but missing something when implementing it is also very simple.
In our implementation, we added constraints to avoid unintended behaviours such as hindering priority jobs. When the priority job cannot fit the machine because of $\off$ machines, switching-off new machines is forbidden.
To prevent ON/OFF cycles, we have also forbidden the switching-off of new machines when machines are being switched-on and when the algorithm plans to switch-on machines later on.
Finally, to limit the number of state switches and to increase the system performance, $\idle$ machines are selected with a higher priority than $\off$ ones.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Evaluation}
\label{sec:evaluation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
We established a benchmark to assess the impact of the $T$ and $t_{idle}$ parameters of the opportunistic shutdown technique.
As we promote research reproducibility,
all material to reproduce our experiment is available online~\cite{gitlab_opportunistic_shutdown_article}.
The evaluation has been conducted in simulation thanks to the
Batsim~\cite{dutot2016batsim} resource management simulator ---
which is based on SimGrid~\cite{simgrid}.
The opportunistic shutdown technique on top of EASY has been implemented in the
Batsched~\cite{gitlab_batsched} project. Experiments have been executed with Batsim commit d6bf186 and Batsched commit 52216b1.
We replayed the two well-known KTH SP2 and SDSC SP2 traces from the Parallel
Workload Archive~\cite{parallel_workload_archive}.
Energy data of the computing machines comes from a series of measures conducted
on the Taurus Grid'5000 test-bed in~\cite{dutot2017towards}. The actual values
are the following:
$t_{\ontooff}   = 6.1~\si{\second}$,
$t_{\offtoon}   = 152~\si{\second}$,
$P_{\computing} = 190.738~\si{\watt}$,
$P_{\idle}      = 95~\si{\watt}$,
$P_{\off}       = 9.75~\si{\watt}$,
$P_{\ontooff}   = 101.639~\si{\watt}$,
$P_{\offtoon}   = 125.197~\si{\watt}$.


In the results presented in the remainder of this paper, EASY results are not directly represented in order to improve the figures readibility. The energy consumption is normalized by the consumption of EASY (without opportunistic shutdown) on each workload: $2.73 \cdot 10^{11}$ J on KTH SP2 and $7.66 \cdot 10^{11}$ J on SDSC SP2. Furthermore, black vertical lines represent the performance of EASY (without opportunistic shutdown).

Figures~\ref{fig_tidle_mwt} and~\ref{fig_tidle_mslowdown} show the impact of
the $t_{idle}$ parameter on energy vs performance tradeoffs for the two traces.
First, we can observe that this technique alone allows significant energy savings, as up to 10\% overall energy savings are conducted on KTH SP2 and up to 15\% on SDSC SP2. However, we can clearly see that the technique can be detrimental for performances. This is especially the case for KTH SP2, where the mean waiting time can rise to 9400 seconds, corresponding to a 48\% increase over classical EASY backfilling. More surprisingly, the technique reduces the mean slowdown on the SDSC SP2 workload. As the slowdown is strongly impacted by the waiting time of jobs with short execution times, we think this is because the technique delays \textit{big} jobs, letting more room to backfill \textit{small} ones. This is compatible with the job distribution of the two studied traces, as SDSC SP2 has more \textit{big} jobs --- in number of requested resources --- than KTH SP2.

While figures~\ref{fig_tidle_mwt} and~\ref{fig_tidle_mslowdown} show aggregated results over the whole workload executions (11 and 24 months for respectively KTH SP2 and SDSC SP2), figures~\ref{fig_results_months_kth} and~\ref{fig_results_months_sdsc} details what happened during the execution of the two traces.
These figures are obtained by aggregating the jobs submitted during the same four-week time frame, rather than the aggregating over the whole trace length. Four-week time frames will simply be referred to as months. We can see that the performance drops on KTH SP2 do not come from a ponctual event but are spread over most months. The technique is less detrimental for performance for the last four months, during which jobs request fewer resources but for longer periods of time --- which is similar to what happens during the months of the SDSC SP2 workload. As SDSC SP2 is more heavily loaded than KTH SP2, we can furthermore see that the technique is more likely to be detrimental for performances when the utilization is high (\eg, months 15 and 17).

We think that the observed performance drops are mainly caused by the lack of robustness of the opportunistic shutdown technique. As described in section~\ref{subsec:opportunistic}, we added multiple guards to avoid detrimental behaviors, but they may not be enough to avoid complex cases. We believe that using more robust indicators than $t_{idle}$ is needed to achieve energy savings at the platform level (in non-clairvoyant scenarios) while maintaining a controlled performance level.

As seen on figures~\ref{fig_tidle_mwt} and~\ref{fig_tidle_mslowdown},
$t_{idle}$ is the main parameter of the technique and
reducing it directly reduces the overall energy consumption.
However, as the impact of $t_{idle}$ on performance highly depends on the workload, we recommend cluster administrators to analyze the impact of several values on their specific workloads before implementing this technique.
We did not observe any impact from the $T$ parameter on the objectives. We have tried other implementation-specific parameters (\eg, to call the opportunistic procedure on each event or only on periodic calls) but could not observe any clear impact from them on the objectives.

\begin{figure}[p]
    \centering
    \includegraphics[width=\linewidth]{../expe/expe_out4/aggregated/europar__energy_mwt__tidle.pdf}
    \caption{Impact of the $t_{idle}$ parameter on Energy vs Mean Waiting Time tradeoffs for the two traces.
             Each point represents a whole trace execution (11 months for KTH SP2, 24 months for SDSC SP2).}
    \label{fig_tidle_mwt}
\end{figure}

\begin{figure}[p]
    \centering
    \includegraphics[width=\linewidth]{../expe/expe_out4/aggregated/europar__energy_mslowdown__tidle.pdf}
    \caption{Impact of the $t_{idle}$ parameter on Energy vs Mean Slowdown tradeoffs for the two traces.
             Each point represents a whole trace execution (11 months for KTH SP2, 24 months for SDSC SP2).}
    \label{fig_tidle_mslowdown}
\end{figure}

\begin{figure}[p]
    \centering
    \includegraphics[width=\linewidth]{../expe/expe_out4/aggregated/months__europar__energy_mwt_algo__kth_sp2.pdf}
    \caption{Impact of the $t_{idle}$ parameter on Energy vs Mean Waiting Time for the KTH SP2 workload. Aggregations are done depending on which month (time frame of four weeks) the jobs have been submitted in.}
    \label{fig_results_months_kth}
\end{figure}

\begin{figure}[p]
    \centering
    \includegraphics[width=\linewidth]{../expe/expe_out4/aggregated/months__europar__energy_mwt_algo__sdsc_sp2.pdf}
    \caption{Impact of the $t_{idle}$ parameter on Energy vs Mean Waiting Time for the SDSC SP2 workload. Aggregations are done depending on which month (time frame of four weeks) the jobs have been submitted in.}
    \label{fig_results_months_sdsc}
\end{figure}

% \begin{figure}[ht]
%     \centering
%     \includegraphics[width=\linewidth]{../expe/expe_out4/aggregated/europar__energy_mwt__period.pdf}
%     \caption{Impact of the $T$ parameter on Energy vs Mean Waiting Time tradeoffs for the two traces.
%              Each point represents a whole trace execution (11 months for KTH SP2, 24 months for SDSC SP2).}
%     \label{fig_period_mwt}
% \end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Related Work}
\label{sec:related_works}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
As we discussed in the introduction, the topic of energy saving in clusters
received an increasing interest from the community.
%
There are dedicated workshops and sessions in most conferences in the domain.
%

In particular, this subject was studied through various theoretical (idealized)
models, see for instance the nice survey of Albers~\cite{albers2010energy},
which analyzed competitive algorithms for both speed-scaling and shutdown
techniques.
%
Another direction is to model the system by Markov
chains~\cite{Herlich:2012:ACA:2208828.2208842}.
%
However, such methods do not lead to implementations.


On a more practical side, speed-scaling and shutdown techniques
have been implemented in the Slurm resource manager and used to respect a
power budget in~\cite{georgiou_adaptive_2015}.
%
Modifications of EASY to respect
an energy budget are conducted in~\cite{dutot2017towards}.
%
Finally, at the application level, Etinski et al. study energy-performance
trade-offs for parallel applications in~\cite{etinski2012understanding}.


Another view of the problem is to consider overprovisioning.
%
Sarood et al. maximize the throughput of moldable jobs under a strict power
budget in~\cite{sarood_maximizing_2014} thanks to linear programs.
%
Patki et al. show in~\cite{patki2016economic} that hardware overprovisioned
systems can be economically viable.


Benoit et al. explore how much energy can be saved thanks to shutdown techniques
in~\cite{benoit:hal-01557025}.
%
They model many practical (physical) constraints that must be dealt
with when large numbers of nodes are turned on and off.
%
They evaluate how much energy can be saved while respecting the
different constraints, considering the job schedule as a fixed input.


The general methodology proposed by Orgerie et al.~\cite{orgerie2008save} aims at reducing energy via shutdown techniques in platforms that rely on advance reservations.
%
Using advance reservations rather than usual HPC jobs allows the scheduler to reduce the impact of bad shutdown decisions, as the brief clairvoyance before actually having to execute the job allows to plan ahead.
%
This paper furthermore investigates predictions methods to
estimate when the next reservation will occur, which could be very beneficial in the problem studied in this article.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion and Future Work}
\label{sec:conclusion}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

We have analyzed the interactions of the opportunistic shutdown technique with
EASY backfilling in this article.
%
This technique is often used to save energy, and we can confirm that it leads to non-negligible energy savings --- up to 30\% have been observed on some months.
%
However, this technique can be very detrimental for performances and we only advise to use it with caution. This is notably the case when the utilization is high, which is common on current HPC platforms.
%
We believe that in this context, using a more robust indicator than the time since which the machines are idle is needed to achieve energy savings while keeping good performances in most cases.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section*{Acknowledgement}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%The work is partially supported by the ANR project MOEBUS.
%Experiments presented in this paper were carried out using the Grid'5000 testbed,
%supported by a scientific interest group hosted by Inria and including CNRS,
%RENATER and several Universities as well as other organizations.

\bibliographystyle{splncs03}
\bibliography{biblio}

\end{document}
